package Questao5;


import java.util.Scanner;

public class Aluno extends Curso{
    protected int matricula;
    protected double[] notas;
    protected double total, media;
    protected Scanner input;

    public Aluno() {
        super();
        notas = new double[4];
        input = new Scanner(System.in);
    }
    // Controle das quatro notas do aluno //
    public void coletorNotas() {
        System.out.print("== Coletor de notas ==\n");
        for (int i = 0; i < notas.length; i++) {
            System.out.printf("Digite a nota %d: ", i+1);
            notas[i] = input.nextDouble();
        }
    }

    @Override
    public void setNome(String anome) {
        super.setNome(anome);
    }

    @Override
    public String getNome() {
        return super.getNome();
    }

    public double calcularMedia() {
        for (double nota : notas) {
            total += nota;
        }
        media = total/notas.length;
        return media;
    }

    public String verficarMedia() {
        if (media < 7.0)
            return "Reprovado";
        else
            return "Aprovado";
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }
}

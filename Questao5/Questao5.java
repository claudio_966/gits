package Questao5;

import java.util.Scanner;

public class Questao5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Aluno[] turma = new Aluno[5];
        Professor profEng = new Professor();

        // Laço que irá varrer o array de alunos //
        for (int contAluno = 0; contAluno < turma.length; contAluno++) {
            turma[contAluno] = new Aluno();
            System.out.printf("== Aluno %d ==\n", contAluno+1);
            System.out.print("Digite seu nome: ");
            turma[contAluno].setNome(input.next());
            System.out.print("Insira sua matrícula: ");
            turma[contAluno].setMatricula(input.nextInt());
            turma[contAluno].coletorNotas();
        }
        // Verifacação dos resultados //
        for (int contResult = 0; contResult < turma.length; contResult++) {
            System.out.println(" == Resultados Finais == ");
            System.out.println("Nome: " + turma[contResult].getNome());
            System.out.printf("Média %.2f\n", turma[contResult].calcularMedia());
            System.out.println("Resultado final: " + turma[contResult].verficarMedia());
        }
        System.out.printf("Média da turma: %.2f", profEng.mediaTurma(turma));
    }
}

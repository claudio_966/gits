package Questao5;

import java.util.Date;

public class Curso {
    private String nome;
    private Date data;
    private double mediasTotais;

    public Curso() {
        mediasTotais = 0;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Date getData() {
        return data;
    }

    public void setNome(String anome) {
        this.nome = anome;
    }

    public String getNome() {
        return nome;
    }

    public double mediaTurma(Aluno[] aAlunos) {
        for (int i = 0; i < aAlunos.length; i++)
            mediasTotais += aAlunos[i].calcularMedia();
        return (mediasTotais / 4.0);
    }
}


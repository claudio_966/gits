package Questao5;

public class Professor extends Curso{
    private String email;
    private String departamento;

    public Professor() {}

    public Professor(String aEmail, String aDepartamento) {
        email = aEmail;
        departamento = aDepartamento;
    }
}

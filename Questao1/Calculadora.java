package Questao1;

import java.util.Scanner;

public class Calculadora {
    private double numero1; // primeiro operando //
    private double numero2; // segundo operando //
    private int operacao; // Operacao a ser realizada //
    private int nFatorial; // numero n do fatorial //
    Scanner input;
    public Calculadora() {
        numero1 = 0;
        numero2 = 0;
        operacao = 0;
        nFatorial = 1;
        input = new Scanner(System.in);
    }

    // Unidade de seleção das operações //
    private void realizarOP() {
        double result = 1;
        switch(operacao) {
            case 1:
                result = this.somar();
                System.out.printf("O resultado de %.2f + %.2f é : %.2f", numero1, numero2, result);
                break;
            case 2:
                result = this.subtrair();
                System.out.printf("O resultado de %.2f - %.2f é : %.2f", numero1, numero2, result);
                break;
            case 3:
                result = this.multi();
                System.out.printf("O resultado de %.2f * %.2f é : %.2f", numero1, numero2, result);
                break;
            case 4:
                result = this.div();
                System.out.printf("O resultado de %.2f / %.2f é : %.2f", numero1, numero2, result);
                break;
            case 5:
                result = this.potencia();
                System.out.printf("O resultado de %.2f^%.2f é : %.2f", numero1, numero2, result);
                break;
            case 6:
                for (int i = nFatorial; i > 1; i--)
                    result *= i;
                System.out.printf("O fatorial de %d é %d", nFatorial, (int)result);
                break;
            case 0:
                System.out.println("Encerrado");
                System.exit(0);
                break;
            default:
                System.out.println("Digite uma opção válida");
                this.coletarN();
        }
    }
    /*
        Espinha dorsal do código
        Controle central do programa
     */
    public void coletarN() {
        System.out.print("Escolha a operação \n > ");
        operacao = input.nextInt();
        if (operacao == 6) {
            System.out.print("Digite um número: ");
            nFatorial = input.nextInt();
        } else {
            System.out.print("Digite o primeiro número: ");
            numero1 = input.nextDouble();
            System.out.print("Digite o segundo número: ");
            numero2 = input.nextDouble();
        }
            if (this.verificadorN()) coletarN();
            else this.realizarOP();
    }

    private double somar() {
        return (numero1 + numero2);
    }

    private double subtrair() {
        return (numero1 - numero2);
    }

    private double multi() {
        return (numero1 * numero2);
    }

    private double div() {
        return (numero1 / numero2);
    }

    private double potencia() {
        return (Math.pow(numero1, numero2));
    }

    /* Verificador de entrada
    * Relacionado a divisão e o fatorial
    * Operações sensíveis a determinados conjuntos de números
    * */
    private boolean verificadorN() {
        if (numero2 == 0 && operacao == 4 || nFatorial < 0 && operacao == 6) {
            System.out.println("Operação inválida. Tente Novamente\n");
            return true;
        }
        return false;
    }

    // Operações válidas //
    public static void MostrarOp() {
        System.out.println("== Operações ==\n" +
                "(1) Soma\n" +
                "(2) Subtração\n" +
                "(3) Multiplicação\n" +
                "(4) Divisão\n" +
                "(5) Potenciação\n" +
                "(6) Fatorial\n" +
                "(0) Sair\n"
        );
    }
}
